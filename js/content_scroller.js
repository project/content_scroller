/**
 * @file
 * Attaches the behaviors for the content scroller module.
 */

(function ($, Drupal) {
  Drupal.behaviors.mScroller = {
    attach: function (context, settings) {
      if ((typeof Drupal.settings.content_scroller_setting != 'undefined')) {
        var selector = Drupal.settings.content_scroller_setting.content_scroller_selector;
        var scroll_type = Drupal.settings.content_scroller_setting.content_scroller_type;
        var scroll_theme = Drupal.settings.content_scroller_setting.content_scroller_theme;
        if (typeof selector != 'undefined') {
          $(selector).mCustomScrollbar({
            axis: 'scroll_type',
            theme: 'scroll_theme',
            scrollbarPosition: 'inside'
          });
        }
      }
    }
  };
})(jQuery, Drupal);
