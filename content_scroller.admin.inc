<?php

/**
 * @file
 * Administration page for Content Scoller.
 */

/**
 * A custom function for custom scrollbar setting form.
 * @return form
 */
function content_scroller_settings() {
  $form = array();
  $form['content_scroller_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('Html selector'),
    '#description' => t('Selector id\'s to apply scroll.Eg. ".content, body".'),
    '#default_value' => variable_get('content_scroller_selector'),
  );
  $form['content_scroller_type'] = array(
    '#type' => 'select',
    '#title' => t('Scrollbar type'),
    '#description' => t('Scrollbar type Eg. Horizonatal Or Vertical'),
    '#options' => array(
      'x' => t('horizontal'),
      'y' => t('vertical'),
      'yx' => t('vertical and horizontal scrollbar'),
    ),
    '#default_value' => variable_get('content_scroller_type'),
  );
  $form['content_scroller_theme'] = array(
    '#type' => 'select',
    '#title' => t('Scrollbar theme'),
    '#description' => t('Scrollbar theme type Eg. light Or dark'),
    '#options' => array(
      'light' => t('light'),
      'dark' => t('dark'),
      'rounded' => t('rounded'),
      '3d' => t('3d'),
      'minimal' > t('minimal'),
    ),
    '#default_value' => variable_get('content_scroller_theme'),
  );
  return system_settings_form($form);
}
